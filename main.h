///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm1 - EE 205 - Spr 2022
// 
//  @file main.h
//
//
// @author Tomoko Austin<tomokoau@hawaii.edu>
// @date   9_mar_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "catDatabase.h"
#include "addCats.h"
#include "deleteCats.h"
#include "reportCats.h"
#include "updateCats.h"
