///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm1 - EE 205 - Spr 2022
// 
//  @file catDatabase.h
//
//
// @author Tomoko Austin<tomokoau@hawaii.edu>
// @date   9_mar_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MAX_CAT_NAME 30
#define MAXIMUM_CAT 1024
enum Breed {UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHINX};
enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK};
enum Gender{UNKNOWN_GENDER, MALE, FEMALE}; 

struct Cat{
   
    char name[MAX_CAT_NAME];
    enum Gender gender;
    enum Breed breed;
    float weight;
    bool isFixed;
    enum Color collarColor1;
    enum Color collarColor2;
    unsigned long long license;
};

extern struct Cat cats[];
extern size_t numCats;

extern char* genderName(const enum Gender gender);

extern char* breedName(const enum Breed breed);

extern char* colorName(const enum Color color);



