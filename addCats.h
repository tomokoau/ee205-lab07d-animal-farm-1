///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm1 - EE 205 - Spr 2022
// 
//  @file addCats.h
//
//
// @author Tomoko Austin<tomokoau@hawaii.edu>
// @date   9_mar_2022
///////////////////////////////////////////////////////////////////////////////#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "catDatabase.h"

int addCat(char cat_name[],enum Gender gender,enum Breed breed, bool is_fixed, float weight,enum Color collarColor1, enum Color collarColor2, unsigned long long license);

