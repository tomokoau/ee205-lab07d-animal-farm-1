


###############################################################################
###         University of Hawaii, College of Engineering
### @brief  Lab 07d - Animalfarm 0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0
###
### @author Tomoko Austin <tomokoau@hawaii.edu>
### @date   02_MAR_2022
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra

TARGET = animalFarm

all: $(TARGET)

addCats.o: addCats.c addCats.h catDatabase.h 
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h 
	$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h 
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h 
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h addCats.h 
	$(CC) $(CFLAGS) -c updateCats.c

validate.o: catDatabase.h
	$(CC) $(CFLAGS) -c validate.c

main.o: main.c addCats.h catDatabase.h deleteCats.h reportCats.h updateCats.h 
	$(CC) $(CFLAGS) -c main.c

animalFarm: main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o 
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o 

test: animalFarm
	./$(TARGET)

clean:
	rm -f $(TARGET) *.o


